import 'package:intl/intl.dart';

class DateTimeUtils {
  static String formatCharMonthDateTime(DateTime dateTime) {
    var date = DateFormat('dd MMMM yyyy').format(dateTime);

    return date;
  }
}
