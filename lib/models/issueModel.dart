class UserIssues {
  final title;
  final update;
  final userIssues users;

  UserIssues({this.title, this.update, this.users});

  factory UserIssues.fromJson(Map<String, dynamic> json) {
    return UserIssues(
        title: json['title'] as String,
        update: DateTime.parse(json['updated_at']),
        users: userIssues.fromJson(json['user']));
  }
}

class userIssues {
  final login;
  final avatar;
  final id;

  userIssues({this.login, this.avatar, this.id});

  factory userIssues.fromJson(Map<String, dynamic> json) {
    return userIssues(
      login: json['login'] as String,
      avatar: json['avatar_url'] as String,
      id: json['id'] as int,
    );
  }
}
