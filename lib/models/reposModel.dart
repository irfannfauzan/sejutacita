class UserRepos {
  final name;
  final created;
  final userRepos users;

  UserRepos({this.name, this.created, this.users});

  factory UserRepos.fromJson(Map<String, dynamic> json) {
    return UserRepos(
      name: json['name'] as String,
      created: DateTime.parse(json['created_at']),
      users: userRepos.fromJson(json['owner']),
    );
  }
}

class userRepos {
  final login;
  final avatar;
  final id;

  userRepos({this.login, this.avatar, this.id});

  factory userRepos.fromJson(Map<String, dynamic> json) {
    return userRepos(
      login: json['login'] as String,
      avatar: json['avatar_url'] as String,
      id: json['id'] as int,
    );
  }
}
