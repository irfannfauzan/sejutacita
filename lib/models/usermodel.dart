class UserModel {
  final login;
  final avatar;

  UserModel({this.login, this.avatar});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      login: json['login'] as String,
      avatar: json['avatar_url'] as String,
    );
  }
}
