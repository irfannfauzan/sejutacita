import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:number_paginator/number_paginator.dart';
import 'package:sejutacita/formatdate.dart';
import 'package:sejutacita/models/usermodel.dart';
import 'package:sejutacita/services/services.dart';
import 'models/issueModel.dart';
import 'models/reposModel.dart';

void main() {
  runApp(Home());
}

PageController _pageController = new PageController(
  keepPage: false,
);

ScrollController _scrollController = new ScrollController();

ScrollController _noScrollController = new ScrollController();

TextEditingController nameSearch = TextEditingController();

int page = 1;

bool isTap = true;

class Home extends StatefulWidget {
  // const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _value;
  int _numPages = 10;

  setSelectedRadio(int val) {
    setState(() {
      _value = val;
      return val;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _value = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent) {
        loadMore();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  void loadMore() async {
    setState(() {
      _scrollController.animateTo(1,
          duration: Duration(milliseconds: 25),
          curve: Curves.fastLinearToSlowEaseIn);
      page++;
    });
  }

  var buttonLoading = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Container(
                      height: 50,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Color(0xFFD4D8DE),
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(color: Color(0xFFF5F5F5))),
                      child: TextField(
                        controller: nameSearch,
                        decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  if (nameSearch != null) {
                                    nameSearch = nameSearch;
                                  }
                                });
                              },
                              icon: Icon(
                                Icons.check,
                                color: Colors.green,
                              ),
                            ),
                            hintText: "Doraemon",
                            icon: Icon(
                              Icons.search,
                              color: Colors.green,
                            ),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Radio(
                          activeColor: Colors.green,
                          value: 0,
                          groupValue: _value,
                          onChanged: (val) {
                            setState(() {
                              print("user");
                              setSelectedRadio(val);
                              _pageController.animateToPage(0,
                                  duration: Duration(milliseconds: 200),
                                  curve: Curves.easeIn);
                            });
                            return;
                          }),
                      Text("User"),
                      Radio(
                          activeColor: Colors.green,
                          value: 1,
                          groupValue: _value,
                          onChanged: (val) {
                            setState(() {
                              print("issues");
                              setSelectedRadio(val);
                              _pageController.animateToPage(1,
                                  duration: Duration(milliseconds: 200),
                                  curve: Curves.easeIn);
                            });
                            return;
                          }),
                      Text("Issues"),
                      Radio(
                          activeColor: Colors.green,
                          value: 2,
                          groupValue: _value,
                          onChanged: (val) {
                            setState(() {
                              print("repos");
                              setSelectedRadio(val);
                              _pageController.animateToPage(2,
                                  duration: Duration(milliseconds: 200),
                                  curve: Curves.easeIn);
                            });
                            return;
                          }),
                      Text("Repositories"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 90,
                        height: 40,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.green)),
                            onPressed: () {
                              setState(() {
                                isTap = true;
                              });
                            },
                            child: Text(
                              "Number Loading",
                              textAlign: TextAlign.center,
                            )),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      SizedBox(
                        height: 40,
                        width: 90,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.green)),
                            onPressed: () {
                              setState(() {
                                isTap = false;
                              });
                            },
                            child: Text(
                              "Lazy Loading",
                              textAlign: TextAlign.center,
                            )),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Expanded(
                      child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (val) {
                      _value = val;
                    },
                    children: [
                      Users(),
                      Isuess(),
                      Repos(),
                    ],
                  )),
                  (isTap == true)
                      ? NumberPaginator(
                          buttonSelectedBackgroundColor: Colors.green,
                          buttonUnselectedForegroundColor: Colors.green,
                          numberPages: _numPages,
                          onPageChange: (int index) {
                            setState(() {
                              page = index + 1;
                            });
                          },
                        )
                      : SizedBox(),
                ],
              ),
            ),
          )),
    );
  }
}

// ignore: must_be_immutable
// Membuat UI Users
class Users extends StatelessWidget {
  String query = nameSearch.text;
  ApiService client = ApiService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.getUsers(query, page),
      builder: (BuildContext context, AsyncSnapshot<List<UserModel>> snapshot) {
        if (snapshot.hasData) {
          List<UserModel> result = snapshot.data;
          return ListView.builder(
            controller:
                (isTap == false) ? _scrollController : _noScrollController,
            scrollDirection: Axis.vertical,
            shrinkWrap: false,
            physics: ScrollPhysics(),
            itemCount: result.length,
            itemBuilder: (context, index) {
              var usersModels = result[index];
              return Column(children: [
                Container(
                  height: 106,
                  width: double.infinity,
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(),
                          child: Stack(
                            children: [
                              Image(image: NetworkImage(usersModels.avatar)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 21.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                usersModels.login,
                                style: TextStyle(
                                  fontSize: 13,
                                ),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ]);
            },
          );
        } else {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [CircularProgressIndicator()],
          ));
        }
      },
    );
  }
}

// ignore: must_be_immutable
// Membuat UI Issues
class Isuess extends StatelessWidget {
  String query = nameSearch.text;
  ApiService client = ApiService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.getIssues(query, page),
      builder:
          (BuildContext context, AsyncSnapshot<List<UserIssues>> snapshot) {
        if (snapshot.hasData) {
          List<UserIssues> result = snapshot.data;
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: false,
            physics: ScrollPhysics(),
            itemCount: result.length,
            itemBuilder: (context, index) {
              var usersModels = result[index];
              return Column(children: [
                Container(
                  height: 106,
                  width: double.infinity,
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(),
                          child: Stack(
                            children: [
                              Image(
                                  image:
                                      NetworkImage(usersModels.users.avatar)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 21.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 100,
                                child: Text(
                                  usersModels.title,
                                  maxLines: 2,
                                  style: TextStyle(
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: Text(
                                    DateTimeUtils.formatCharMonthDateTime(
                                        usersModels.update),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 33, top: 4),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "ID Issues :",
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  ),
                                  Text(
                                    "${usersModels.users.id}",
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ]);
            },
          );
        } else {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [CircularProgressIndicator()],
          ));
        }
      },
    );
  }
}

// ignore: must_be_immutable
// Membuat UI Repos
class Repos extends StatelessWidget {
  String query = nameSearch.text;
  ApiService client = ApiService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.getRepos(query, page),
      builder: (BuildContext context, AsyncSnapshot<List<UserRepos>> snapshot) {
        if (snapshot.hasData) {
          List<UserRepos> result = snapshot.data;
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: false,
            physics: ScrollPhysics(),
            itemCount: result.length,
            itemBuilder: (context, index) {
              var usersModels = result[index];
              return Column(children: [
                Container(
                  height: 106,
                  width: double.infinity,
                  decoration: BoxDecoration(),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(),
                          child: Stack(
                            children: [
                              Image(
                                  image:
                                      NetworkImage(usersModels.users.avatar)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 21.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 100,
                                child: Text(
                                  usersModels.name,
                                  maxLines: 2,
                                  style: TextStyle(
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: Text(
                                    DateTimeUtils.formatCharMonthDateTime(
                                        usersModels.created),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 33, top: 4),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "ID Repo :",
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  ),
                                  Text(
                                    "${usersModels.users.id}",
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 13,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ]);
            },
          );
        } else {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [CircularProgressIndicator()],
          ));
        }
      },
    );
  }
}
