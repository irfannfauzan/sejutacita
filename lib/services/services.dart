import 'dart:convert';
import 'package:http/http.dart';
import 'package:sejutacita/models/issueModel.dart';
import 'package:sejutacita/models/reposModel.dart';
import 'package:sejutacita/models/usermodel.dart';

class ApiService {
  final url = "https://api.github.com/search/users?q=";
  final urlIssues = "https://api.github.com/search/issues?q=";
  final urlRepos = "https://api.github.com/search/repositories?q=";
  final params = "&page=";

  Future<List<UserModel>> getUsers(String query, int page) async {
    if (query == '') {
      query = "Doraemon";
    } else {
      query = query;
    }
    Response res = await get(url + query + params + page.toString());
    print(url + query + params + page.toString());
    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);
      List<dynamic> body = json['items'];
      List<UserModel> results =
          body.map((dynamic item) => UserModel.fromJson(item)).toList();
      print("Success getUsers");
      return results;
    } else {
      print("Reach Limited API");
      throw Exception();
    }
  }

  Future<List<UserIssues>> getIssues(String query, int page) async {
    if (query == '') {
      query = "Doraemon";
    } else {
      query = query;
    }
    Response res = await get(urlIssues + query + params + page.toString());
    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);
      List<dynamic> body = json['items'];
      List<UserIssues> results =
          body.map((dynamic item) => UserIssues.fromJson(item)).toList();
      print("Success getIssues");
      return results;
    } else {
      print("Reach Limited API");
      throw Exception();
    }
  }

  Future<List<UserRepos>> getRepos(String query, int page) async {
    if (query == '') {
      query = "Doraemon";
    } else {
      query = query;
    }
    Response res = await get(urlRepos + query + params + page.toString());

    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);
      List<dynamic> body = json['items'];
      List<UserRepos> results =
          body.map((dynamic item) => UserRepos.fromJson(item)).toList();
      print("Success getRepos");
      return results;
    } else {
      print("Reach Limited API");
      throw Exception();
    }
  }
}
